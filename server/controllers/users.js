const Model = require('../model');
const { Users, Courses } = Model;

const usersController = {
  // 查询所有用户
  all(req, res) {
    Users.find({}, (err, users) => {
      if (err) {
        res.json({
          code: 500,
          msg: '服务器错误',
        });
      } else {
        res.json({
          code: 200,
          msg: '查询成功',
          data: users,
        });
      }
    });
  },
  // 获取账号个人信息
  get_userinfo(req, res) {
    if (!req.query['email']) {
      res.status(400).json({
        code: 400,
        msg: '查询失败',
        err: '请发送对应的账号email参数',
      });
    }
    else {
      Users.findOne({ 'email': req.query['email'] }, { password: 0 }, (err, users) => {
        if (err) {
          res.status(400).json({
            code: 400,
            msg: '查询失败',
            err: err,
          });
        } else if (users == null) {
          res.json({
            code: 400,
            msg: '查询失败',
            err: '没有找到该用户'
          });
        } else {
          res.json({
            code: 200,
            msg: '查询成功',
            data: users,
          });
        }
      });
    }
  },
  // 添加账号个人信息
  add_userinfo(req, res) {
    if (!req.body['email']) {
      res.status(400).json({
        code: 400,
        msg: '查询失败',
        err: '请发送对应的账号email参数',
      });
    }
    else {
      var mquery = Users.findOne({ 'email': req.body['email'] });
      mquery.update({
        $set: {
          name: req.body['name'],
          year: req.body['year'],
          date: req.body['date'],
          region: req.body['region'],
          desc: req.body['desc'],
          grade: req.body['grade'],
        },
      });
      mquery.exec((err) => {
        if (err) {
          res.status(400).json({
            code: 400,
            msg: '修改失败',
            err: err,
          });
        } else {
          res.json({
            code: 200,
            msg: '添加成功'
          });
        }
      });

    }

  },
  // 请求课表，学号版
  get_timetable(req, res) {
    if (!req.query['account']) {
      res.status(400).json({
        code: 400,
        msg: '查询失败',
        err: '请发送对应的账号account参数',
      });
    }
    else {
      Users.findOne({ account: req.query['account'] }, { password: 0 }, (err, users) => {
        if (err) {
          res.status(400).json({
            code: 400,
            msg: '查询失败',
            err: err,
          });
        } else if (users == null) {
          res.json({
            code: 400,
            msg: '查询失败',
            err: '没有找到该用户'
          });
        } else {
          Courses.find({
            _id: {
              $in: users['courses']
            }
          }, (err, courses) => {
            if (err) {
              res.status(400).json({
                code: 400,
                msg: '查询失败',
                err: err,
              });
            } else {
              res.json({
                code: 200,
                msg: '查询成功',
                num: courses.length,
                courses: courses,
              });
            }
          });
        }
      });
    }
  },
  // 请求课表，邮箱版
  get_timetable2(req, res) {
    if (!req.query['email']) {
      res.status(400).json({
        code: 400,
        msg: '查询失败',
        err: '请发送对应的email参数',
      });
    }
    else {
      Users.findOne({ email: req.query['email'] }, { password: 0 }, (err, users) => {
        if (err) {
          res.status(400).json({
            code: 400,
            msg: '查询失败',
            err: err,
          });
        } else if (users == null) {
          res.json({
            code: 400,
            msg: '查询失败',
            err: '没有找到该用户'
          });
        } else {
          Courses.find({
            _id: {
              $in: users['courses']
            }
          }, (err, courses) => {
            if (err) {
              res.status(400).json({
                code: 400,
                msg: '查询失败',
                err: err,
              });
            } else {
              res.json({
                code: 200,
                msg: '查询成功',
                num: courses.length,
                courses: courses,
              });
            }
          });
        }
      });
    }
  },
  // 添加课表
  add_timetable(req, res) {
    if (!req.body['email']) {
      res.status(400).json({
        code: 400,
        msg: '查询失败',
        err: '请发送对应的email参数',
      });
    }
    else {
      var mquery = Users.findOne({ 'email': req.body['email'] });
      mquery.update({
        $push: {
          courses: req.body['cid']
        },
      });
      mquery.exec((err) => {
        if (err) {
          res.status(400).json({
            code: 400,
            msg: '修改失败',
            err: err,
          });
        } else {
          res.json({
            code: 200,
            msg: '添加成功'
          });
        }
      });
    }
  },
  // 删除课表
  delete_timetable(req, res) {
    if (!req.body['email']) {
      res.status(400).json({
        code: 400,
        msg: '查询失败',
        err: '请发送对应的email参数',
      });
    }
    else {
      var mquery = Users.findOne({ 'email': req.body['email'] });
      mquery.update({
        $pull: {
          courses: req.body['cid']
        },
      });
      mquery.exec((err) => {
        if (err) {
          res.status(400).json({
            code: 400,
            msg: '修改失败',
            err: err,
          });
        } else {
          res.json({
            code: 200,
            msg: '删除成功'
          });
        }
      });
    }
  },
  // 添加用户收藏课程
  add_collection(req, res) {
    if (!req.body['email']) {
      res.status(400).json({
        code: 400,
        msg: '查询失败',
        err: '请发送对应的email参数',
      });
    }
    else {
      var mquery = Users.findOne({ 'email': req.body['email'] });
      mquery.update({
        $push: {
          favorites_courses: req.body['cid']
        },
      });
      mquery.exec((err) => {
        if (err) {
          res.status(400).json({
            code: 400,
            msg: '修改失败',
            err: err,
          });
        } else {
          res.json({
            code: 200,
            msg: '添加成功'
          });
        }
      });
    }
  },
  // 删除用户收藏课程
  delete_collection(req, res) {
    if (!req.body['email']) {
      res.status(400).json({
        code: 400,
        msg: '查询失败',
        err: '请发送对应的email参数',
      });
    }
    else {
      var mquery = Users.findOne({ 'email': req.body['email'] });
      console.log(mquery);
      mquery.update({
        $pull: {
          favorites_courses: req.body['cid']
        },
      });
      mquery.exec((err) => {
        if (err) {
          res.status(400).json({
            code: 400,
            msg: '修改失败',
            err: err,
          });
        } else {
          res.json({
            code: 200,
            msg: '删除成功'
          });
        }
      });
    }
  },
  // 添加用户头像链接
  add_avatar(req, res) {
    if (!req.body['email']) {
      res.status(400).json({
        code: 400,
        msg: '查询失败',
        err: '请发送对应的email参数',
      });
    }
    else {
      var mquery = Users.findOne({ 'email': req.body['email'] });
      mquery.update({
        $set: {
          avatar: req.body['avatar']
        },
      });
      mquery.exec((err) => {
        if (err) {
          res.status(400).json({
            code: 400,
            msg: '修改失败',
            err: err,
          });
        } else {
          res.json({
            code: 200,
            msg: '添加成功'
          });
        }
      });
    }
  },
  // 获取用户收藏课程
  get_collection(req, res) {
    if (!req.query['email']) {
      res.status(400).json({
        code: 400,
        msg: '查询失败',
        err: '请发送对应的email参数',
      });
    }
    else {
      Users.findOne({ email: req.query['email'] }, (err, users) => {
        if (err) {
          res.status(400).json({
            code: 400,
            msg: '查询失败',
            err: err,
          });
        } else if (users == null) {
          res.json({
            code: 400,
            msg: '查询失败',
            err: '没有找到该用户'
          });
        } else {
          Courses.find({
            _id: {
              $in: users['favorites_courses']
            }
          }, (err, courses) => {
            if (err) {
              res.status(400).json({
                code: 400,
                msg: '查询失败',
                err: err,
              });
            } else {
              res.json({
                code: 200,
                msg: '查询成功',
                num: courses.length,
                collections: courses,
              });
            }
          });
        }
      });
    }
  },
};

module.exports = usersController;