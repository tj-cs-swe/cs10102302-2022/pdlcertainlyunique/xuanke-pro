const { query } = require('express');
const Model = require('../model');
const { Courseinfos } = Model;

const coursesinfoController = {
  // 获取课程信息
  get_courseinfo(req, res) {
    var mquery = Courseinfos.findOne({ course_id: req.query['course_id'] });
    mquery.exec((err, courseinfo) => {
      if (err) {
        res.status(400).json({
          code: 400,
          msg: '查询失败',
          err: err,
        });
      } 
      else if (courseinfo == null) {
        res.status(400).json({
          code: 400,
          msg: '查询失败',
          err: '没有该课程',
        });
      }
      else {
        res.json({
          code: 200,
          msg: courseinfo['msg'],
          courseTitle: courseinfo['courseTitle'],
          courseTime: courseinfo['courseTime'],
          credit: courseinfo['credit'],
          teacher: courseinfo['teacher'],
          assessment: courseinfo['assessment'],
          teacher_message: courseinfo['teacher_message'],
          hasMessage: Boolean(courseinfo['teacher_message'] != null),
          course_intro: courseinfo['course_intro'],
          outline: courseinfo['outline'],
          labels: courseinfo['labels'],
        });
      }
    });
  },
  // 提交课程信息
  update_courseinfo(req, res) {
    console.log(req.body);
    var mquery = Courseinfos.findOne({ course_id: req.body['course_id'] });
    mquery.exec((err, courseinfo) => {
      if (err) {
        res.status(400).json({
          code: 400,
          msg: '查询失败',
          err: err,
        });
      } else {
        if (courseinfo != null) {
          console.log(courseinfo);
          var mquery = Courseinfos.findOne({ course_id: req.body['course_id'] });
          mquery.update({
            $set: {
              course_intro: req.body['course_intro'],
              teacher_message: req.body['teacher_message'],
              outline: req.body['outline'],
              labels: req.body['labels'],
            }
          });
          mquery.exec((err, question) => {
            if (err) {
              res.status(400).json({
                code: 400,
                msg: '修改失败',
                err: err,
              });
            } else {
              res.json({
                code: 200,
                msg: '修改成功'
              });
            }
          });
        } else {
          var courseinfo2 = new Courseinfos({
            course_id: req.body['course_id'],
            course_intro: req.body['course_intro'],
            teacher_message: req.body['teacher_message'],
            outline: req.body['outline'],
            labels: req.body['labels'],
          });
          courseinfo2.save((err) => {
            if (err) {
              res.status(400).json({
                code: 400,
                msg: '添加失败',
                err: err,
              });
            } else {
              res.json({
                code: 200,
                msg: '添加成功',
                _id: courseinfo2['_id']
              });
            }
          });
        }
      }
    });

  }
};

module.exports = coursesinfoController;