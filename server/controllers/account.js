const Model = require('../model');
const { Users } = Model;
const tokenApi = require('./token_vertify');

const AccountController = {
  // 添加用户
  add(req, res) {
    const newUser = new Users(req.body);
    // 查询用户是否存在
    Users.findOne({ email: newUser.email }, (err, user) => {
      if (err) {
        res.status(500).json({
          code: 500,
          msg: '服务器错误',
        });
      }
      else if (user) {
        res.json({
          code: 400,
          msg: '用户已存在',
        });
      }
      else {
        newUser.save((err, user) => {
          if (err) {
            res.json({
              code: 500,
              msg: '服务器错误',
            });
          } else {
            Users.findOne({ _id: user._id }, (err, user) => {
              res.json({
                code: 200,
                msg: '添加成功',
                user: user,
              });
            });
          }
        });
      }
    });
  },
  // 登录验证
  login(req, res) {
    let { email, password } = req.query;
    Users.findOne({ email }, (err, user) => {
      if (err) {
        res.status(500).json({
          code: 500,
          msg: '服务器错误',
        });
      }
      else if (!user) {
        res.json({
          code: 404,
          msg: '用户不存在',
        });
      }
      else if (user.password !== password) {
        res.json({
          code: 400,
          msg: '密码不正确',
        });
      }
      else {
        tokenApi.setToken(email).then(token => {
          res.json({
            code: 200,
            msg: '登录成功',
            username: user.name,
            role: user.role,
            avatar: user.avatar,
            email: user.email,
            token: token,
          });
        });
      }
    });
  },
  // 重置密码
  resetPassword(req, res) {
    let { email, password } = req.body;
    Users.findOne({ email }, (err, user) => {
      if (err) {
        res.status(500).json({
          code: 500,
          msg: '服务器错误',
        });
      }
      else if (!user) {
        res.json({
          code: 404,
          msg: '账号不存在',
        });
      }
      else {
        Users.updateOne({ email }, { password }, (err, user) => {
          if (err) {
            res.status(500).json({
              code: 500,
              msg: '服务器错误',
            });
          }
          else {
            res.json({
              code: 200,
              msg: '修改成功',
            });
          }
        });
      }
    });
    
  },
  // 修改密码
  changePassword(req, res) {
    let { email, password, new_password } = req.body;
    Users.findOne({ email }, (err, user) => {
      if (err) {
        res.status(500).json({
          code: 500,
          msg: '服务器错误',
        });
      }
      else if (!user) {
        res.json({
          code: 404,
          msg: '账号不存在',
        });
      }
      else if (user.password !== password) {
        res.json({
          code: 400,
          msg: '原密码不正确',
        });
      }
      else {
        Users.updateOne({ email }, { new_password }, (err, user) => {
          if (err) {
            res.status(500).json({
              code: 500,
              msg: '服务器错误',
            });
          }
          else {
            res.json({
              code: 200,
              msg: '修改成功',
            });
          }
        });
      }
    });
  }
};  

module.exports = AccountController;