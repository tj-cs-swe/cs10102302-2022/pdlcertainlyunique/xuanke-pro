const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const cors = require('cors');
const session = require('express-session');

const apiRouter = require('./routes/api');
const mongoose = require('mongoose');


var app = express();
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// 配置session
app.use(session({
  secret: 'raft keyboard cat',
  resave: false,
  saveUninitialized: true,
  cookie: {
    maxAge: 1000 * 60 * 60 * 24 * 7,
  },
}));

// 连接数据库
mongoose.connect('mongodb://127.0.0.1:27017/xuanke-pro');
let db = mongoose.connection;
db.on('error', () => {
  console.error('mongodb: 连接错误');
});


// 设置跨域访问
app.use(cors({
  origin: 'http://localhost:8080',
  credentials: true,
}));

app.use('/images',express.static('images'));

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/api', apiRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;